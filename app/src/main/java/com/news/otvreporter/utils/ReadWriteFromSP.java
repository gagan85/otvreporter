package com.news.otvreporter.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by m81361 on 24-02-2016.
 */
public class ReadWriteFromSP {


    public static void writeToSP(Context context, String key, Object value) {
        SharedPreferences preference;
        preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        }
        editor.commit();
    }

    public static String readStringFromSP(Context context, String key) {
        SharedPreferences preference;
        preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getString(key, "");
    }

    public static String readFontFromSP(Context context, String key) {
        SharedPreferences preference;
        preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getString(key, "18");
    }

    public static int readIntegerFromSP(Context context, String key) {
        SharedPreferences preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getInt(key, Integer.MIN_VALUE);

    }

    public static long readLongFromSP(Context context, String key) {
        SharedPreferences preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getLong(key, 0);

    }

    public static boolean readBooleanFromSP(Context context, String key) {
        SharedPreferences preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getBoolean(key, false);

    }

    public static boolean readNotificationBooleanFromSP(Context context, String key) {
        SharedPreferences preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getBoolean(key, true);

    }
    public static int readIntegerFromSPWithZeroDefaultValue(Context context, String key) {
        SharedPreferences preference = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preference.getInt(key, 0);

    }

}
