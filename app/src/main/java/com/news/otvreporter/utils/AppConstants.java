package com.news.otvreporter.utils;

public interface AppConstants {


    String SERVER_NOT_RESPONDING = "server not responding";
    String NETWORK_NOT_AVAILABLE = "Network not available";
    String JSONEXPECTION = "JSONEXPECTION";

    String POST_Story_URL = "";
}
