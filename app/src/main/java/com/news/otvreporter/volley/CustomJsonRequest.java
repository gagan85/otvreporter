package com.news.otvreporter.volley;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import com.news.otvreporter.AppController;
import com.news.otvreporter.utils.AppConstants;
import com.news.otvreporter.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class CustomJsonRequest {


    public static final int GET = 0;
    public static final int POST = 1;
    OnServerResponse onServerResponse;
    ProgressDialog mProgressDialog;
    Context context;


    public CustomJsonRequest(Context context, OnServerResponse onServerResponse) {
        this.context = context;
        this.onServerResponse = onServerResponse;
    }

    private static boolean isJSONObject(String data) throws JSONException {
        Object json = new JSONTokener(data).nextValue();
        if (json instanceof JSONObject)
            return true;
        return false;
    }

    private void showProgressDialog() {
        try {
            if (context instanceof AppCompatActivity && !((AppCompatActivity) context).isFinishing()) {
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setMessage("Please wait!");
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dissmisProgressDialog() {
        try {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDataFromServer(int requestType, String tag, String url, JSONObject body, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        switch (requestType) {
            case GET:
                getJsonFromServerThroughGet(tag, url, headers, isCachedData, isProgressDialog);
                break;
            case POST:
                getJsonFromServerThroughPost(tag, url, headers, body, isCachedData, isProgressDialog);
                break;
        }

    }

    public void getJsonFromLocal(final String tag, final String url) {
        try {
            if (onServerResponse != null && onServerResponse instanceof OnServerResponse) {
                onServerResponse.getJsonFromServer(true, url, new JSONObject(readFile(url)), null);
            }
        } catch (Exception e) {
        }
    }

    private void getJsonFromServerThroughGet(final String tag, final String url, final HashMap<String, String> headers, final boolean isCachedData, final boolean isProgressDialog) {

        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        cache.clear();
        HttpURLConnection.setFollowRedirects(true);
        // making fresh volley request and getting json
       /* HttpMetric metric =
                FirebasePerformance.getInstance().newHttpMetric("https://www.google.com",
                        FirebasePerformance.HttpMethod.GET);
        metric.start();*/
        MyJsonObjectRequest jsonReq = new MyJsonObjectRequest(Method.GET,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    VolleyLog.d(tag, "Response: " + response.toString());
                    if (isProgressDialog)
                        dissmisProgressDialog();
                    if (response != null) {
                        if (onServerResponse != null && onServerResponse instanceof OnServerResponse) {
                            onServerResponse.getJsonFromServer(true, url, response, null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("onError Hyperlink 11", "Error: " + error.getMessage());
                dissmisProgressDialog();

                if (onServerResponse != null && onServerResponse instanceof OnServerResponse) {
                    String strError = "String cannot be converted to JSONObject";
                    if (!ConnectionDetector.isConnectingToInternet(context)) {
                        strError = AppConstants.NETWORK_NOT_AVAILABLE;
                    } else {
                        if (error.getMessage() == null) {
                            strError = AppConstants.SERVER_NOT_RESPONDING;
                        } else if (error.getMessage().contains("String cannot be converted to JSONObject")) {
                            strError = AppConstants.JSONEXPECTION;
                        } else if (error.getMessage().contains("java.net.UnknownHostException")) {
                            strError = AppConstants.SERVER_NOT_RESPONDING;
                        } else {
                            strError = AppConstants.SERVER_NOT_RESPONDING;
                        }
                    }
                    onServerResponse.getJsonFromServer(false, url, null, strError);
                }

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (headers != null) {
                    return headers;
                }
                return super.getHeaders();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                if (response != null) {
                    int statusCode = response.statusCode;
                    Log.e("STATUS_CODE", statusCode + "");
                }
                return super.parseNetworkResponse(response);
            }
        };
        // Adding request to volley request queue


        AppController.getInstance().addToRequestQueue(jsonReq);
        if (isProgressDialog)
            showProgressDialog();

        /*metric.stop();*/
    }

    private void getJsonFromServerThroughPost(final String tag, final String url, final HashMap<String, String> headers, final JSONObject jsonObject, boolean isCachedData, final boolean isProgressDialog) {
        Cache cache = AppController.getInstance().getRequestQueue().getCache();
        cache.clear();
        HttpURLConnection.setFollowRedirects(true);
        // making fresh volley request and getting json
        MyJsonObjectRequest jsonReq = new MyJsonObjectRequest(Method.POST,
                url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                VolleyLog.d(tag, "Response: " + response.toString());
                if (isProgressDialog)
                    dissmisProgressDialog();
                if (response != null) {
                    if (onServerResponse != null && onServerResponse instanceof OnServerResponse) {
                        onServerResponse.getJsonFromServer(true, url, response, null);
                    }
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(tag, "Error: " + error.getMessage());
                dissmisProgressDialog();
                if (onServerResponse != null && onServerResponse instanceof OnServerResponse) {
                    String strError = "String cannot be converted to JSONObject";
                    if (!ConnectionDetector.isConnectingToInternet(context)) {
                        strError = AppConstants.NETWORK_NOT_AVAILABLE;
                    } else {
                        if (error.getMessage() == null) {
                            strError = AppConstants.SERVER_NOT_RESPONDING;
                        } else if (error.getMessage().contains("String cannot be converted to JSONObject")) {
                            strError = AppConstants.JSONEXPECTION;
                        } else if (error.getMessage().contains("java.net.UnknownHostException")) {
                            strError = AppConstants.SERVER_NOT_RESPONDING;
                        } else {
                            strError = AppConstants.SERVER_NOT_RESPONDING;
                        }
                    }
                    onServerResponse.getJsonFromServer(false, url, null, strError);
                }

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (headers != null) {
                    return headers;
                }
                return super.getHeaders();
            }
        };

        // Adding request to volley request queue
        AppController.getInstance().addToRequestQueue(jsonReq);
        if (isProgressDialog)
            showProgressDialog();

    }

    private String readFile(String fileName) {
        String response = "";
        try {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(fileName);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String string = bufferedReader.readLine();
            while (string != null) {
                stringBuilder.append(string + "\n");
                string = bufferedReader.readLine();

            }
            response = stringBuilder.toString();
        } catch (Exception e) {
        }
        return response;
    }

    public String openFileToString(byte[] _bytes) {
        String file_string = "";

        try {
            for (int i = 0; i < _bytes.length; i++) {
                file_string += (char) _bytes[i];
            }
        } catch (Exception e) {
        }

        return file_string;
    }


    public interface OnServerResponse {
        public void getJsonFromServer(boolean flag, String url, JSONObject jsonObject, String error);
    }
}
