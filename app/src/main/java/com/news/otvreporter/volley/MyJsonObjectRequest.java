package com.news.otvreporter.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;

public class MyJsonObjectRequest extends JsonObjectRequest {

    public MyJsonObjectRequest(int method, String url, JSONObject jsonRequest,
                               Listener<JSONObject> listener, ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        // TODO Auto-generated constructor stub
    }

    public MyJsonObjectRequest(String url, JSONObject jsonRequest,
                               Listener<JSONObject> listener, ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
        // TODO Auto-generated constructor stub
    }


    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {

            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            if (!isJSONObject(jsonString)) {
                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArray = new JSONArray(jsonString);
                jsonObject.put("result", jsonArray);
                //return Response.success(jsonObject, MyHttpHeaderParser.parseIgnoreCacheHeaders(response));
                return Response.success(jsonObject,HttpHeaderParser.parseCacheHeaders(response));
            }
//            return Response.success(new JSONObject(jsonString), MyHttpHeaderParser.parseIgnoreCacheHeaders(response));
	            return Response.success(new JSONObject(jsonString),HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    public static boolean isJSONObject(String data) throws JSONException {
        Object json = new JSONTokener(data).nextValue();
        if (json instanceof JSONObject)
            return true;
//		else if (json instanceof JSONArray)
        return false;
    }


}
