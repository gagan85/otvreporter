package com.news.otvreporter;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();
    private static final String namespace = "AndroidTracker";
    private static final String appId = "lm";
    /* static ConfigPojo configPojo;*/
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
  //  private FirebaseAnalytics mFirebaseAnalytics;



    public static synchronized AppController getInstance() {
        return mInstance;
    }

   /* public ConfigPojo getConfigPojo() {
        if (configPojo == null) {
            try {
                String configData = Utils.readStringFromSP(AppController.this, AppConstants.KEY_CONFIG_DATA);
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(configData);
                Gson gson = new Gson();
                configPojo = gson.fromJson(jsonObject.toString(), ConfigPojo.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return configPojo;
    }*/

    /*public static void setConfigPojo(ConfigPojo configPojo) {
        AppController.configPojo = configPojo;
    }*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
       /* PublisherConfiguration myPublisherConfig = new PublisherConfiguration.Builder()
                .publisherId(AppConstants.COMSCORE_COUSTMER_C2)
                .publisherSecret(AppConstants.COMSCORE_PUBLISER_SECRET)
                .usagePropertiesAutoUpdateMode(UsagePropertiesAutoUpdateMode.FOREGROUND_ONLY)
                .build();
        Analytics.getConfiguration().addClient(myPublisherConfig);
        Analytics.start(this.getApplicationContext());
        sAnalytics = GoogleAnalytics.getInstance(this);*/


    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


   /* // firebase instance
    synchronized public FirebaseAnalytics getDefaultTracker() {
        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }
        return mFirebaseAnalytics;
    }*/


}
