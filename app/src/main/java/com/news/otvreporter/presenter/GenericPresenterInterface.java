package com.news.otvreporter.presenter;

import org.json.JSONObject;

import java.util.HashMap;

public interface GenericPresenterInterface {

    void getData(int requestType, String tag, String url, JSONObject body, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog);

}
