package com.news.otvreporter.presenter;


import org.json.JSONObject;

public interface GenericViewInterface {
    void getData(JSONObject jsonObjectl, String url);

    void onError(String message, String url);
}
