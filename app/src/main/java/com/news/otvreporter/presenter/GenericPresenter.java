package com.news.otvreporter.presenter;

import android.content.Context;


import com.news.otvreporter.volley.CustomJsonRequest;

import org.json.JSONObject;

import java.util.HashMap;

public class GenericPresenter implements GenericPresenterInterface, CustomJsonRequest.OnServerResponse {

    Context context;
    CustomJsonRequest customJsonRequest;
    String dataUrl = "";
    private GenericViewInterface homeViewInterface;

    public GenericPresenter(Context context, GenericViewInterface homeViewInterface) {
        this.homeViewInterface = homeViewInterface;
        customJsonRequest = new CustomJsonRequest(context, this);
    }

    @Override
    public void getData(int requestType, String tag, String url, JSONObject body, HashMap<String, String> headers, boolean isCachedData, boolean isProgressDialog) {
        dataUrl = url;
        customJsonRequest.getDataFromServer(requestType, tag, url, body, headers, isCachedData, isProgressDialog);
    }


    @Override
    public void getJsonFromServer(boolean flag, String url, JSONObject jsonObject, String error) {
        if (flag && jsonObject != null) {
            homeViewInterface.getData(jsonObject, url);
        } else {
            homeViewInterface.onError(error, url);
        }
    }

}
